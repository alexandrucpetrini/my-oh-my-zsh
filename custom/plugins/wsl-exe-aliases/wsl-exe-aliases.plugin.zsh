# Add your own custom plugins in the custom/plugins directory. Plugins placed
# here will override ones with the same name in the main plugins directory.
#

alias vagrant="vagrant.exe"
#alias aws="aws.exe"
#alias terraform="terraform.exe"
alias clip=clip.exe
alias choco="choco.exe"
#alias powershell=powershell.exe
#alias eksctl="eksctl.exe"
#alias eks=eksctl
#alias docker-compose="docker-compose.exe"
#alias kompose="kompose.exe"
#alias glcoud="gcloud.cmd"
alias minikube="minikube.exe"
#alias kubectl="kubectl.exe"
#alias helm="helm.exe"
#alias kompose="kompose.exe"
alias hadoop="docker run --rm --name hadoop apache/hadoop:3 hadoop"
#alias hadoop="docker run --rm --name hadoop apache/hadoop-runner hadoop"
