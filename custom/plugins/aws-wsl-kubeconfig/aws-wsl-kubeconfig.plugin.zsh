# Add your own custom plugins in the custom/plugins directory. Plugins placed
# here will override ones with the same name in the main plugins directory.
export KUBECONFIG=$KUBECONFIG:/mnt/c/Users/apetrini/.kube/config

alias eksctl=eksctl.exe
alias eks=eksctl
