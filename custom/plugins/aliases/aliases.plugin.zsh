#! /bin/zsh
alias ghis='history | grep'
alias eghis='history | grep -E'
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias set_stty='stty rows 50 columns 212'
