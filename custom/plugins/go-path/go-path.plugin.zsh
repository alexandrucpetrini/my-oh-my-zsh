#! /bin/zsh
# Add your own custom plugins in the custom/plugins directory. Plugins placed
# here will override ones with the same name in the main plugins directory.

#export GOPATH="/opt/go"
#export PATH="$GOPATH/bin:$PATH"
export GOROOT=$HOME/go
export GOPATH=$HOME/go/bin
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
