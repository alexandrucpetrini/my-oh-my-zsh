# Authors:
# https://github.com/tristola
#
# Docker-compose related zsh aliases

# Aliases ###################################################################

# Use dco as alias for docker-compose, since dc on *nix is 'dc - an arbitrary precision calculator'
# https://www.gnu.org/software/bc/manual/dc-1.05/html_mono/dc.html

alias dco='docker-compose'

alias dcb='docker-compose build'
alias dce='docker-compose exec'
alias dcps='docker-compose ps'
alias dcrestart='docker-compose restart'
alias dcrm='docker-compose rm'
alias dcr='docker-compose run'
alias dcstop='docker-compose stop'
alias dcupo='docker-compose up --remove-orphans'
alias dcupbo='docker-compose up --build --remove-orphans'
alias dcupdbo='docker-compose up --build --remove-orphans'
alias dcupd='docker-compose up -d --remove-orphans'
alias dcdn='docker-compose down --remove-orphans'
alias dcup='docker-compose up'
alias dcupb='docker-compose up --build'
alias dcupd='docker-compose up -d'
alias dcupdb='docker-compose up -d --build'
alias dcdn='docker-compose down'
alias dcl='docker-compose logs'
alias dclf='docker-compose logs -f'
alias dcpull='docker-compose pull'
alias dcstart='docker-compose start'
alias dck='docker-compose kill'

alias dcreco='docker-compose up -d --force-recreate --remove-orphans'
alias dcrec='docker-compose up -d --force-recreate'
alias dcrec='docker-compose up -d --force-recreate --remove-orphans'
