#! /bin/zsh
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/profile ]; then
. /etc/profile
fi
if [ -f /etc/profile.d/zzz-personal-exports.sh ]; then
. /etc/profile.d/zzz-personal-exports.sh
fi
if [ -f ~/.profile ]; then
. ~/.profile
fi
if [ -f ~/.bash_profile ]; then
. ~/.bash_profile
fi
for completion in /etc/bash_completion.d/*; do
    [ -f "$completion" ] && source $completion
done
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi
if [ -f ~/.alias ]; then
. ~/.alias
fi
