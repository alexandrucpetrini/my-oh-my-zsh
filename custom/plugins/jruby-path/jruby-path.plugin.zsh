#! /bin/zsh
# Add your own custom plugins in the custom/plugins directory. Plugins placed
# here will override ones with the same name in the main plugins directory.
#
# /opt/bin/ path
if [ -d "/opt/jruby/bin" ] ; then
    PATH="/opt/jruby/bin:$PATH"
fi
